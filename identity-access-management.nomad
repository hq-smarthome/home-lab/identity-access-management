job "identity-access-management" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  group "keycloak" {
    count = 1

    network {
      port "keycloak" { to = 8080 }
    }

    task "keycloak" {
      driver = "docker"

      vault {
        policies = ["identity-access-management"]
      }

      constraint {
        attribute = "${attr.cpu.arch}"
        value = "amd64"
      }

      config {
        image = "quay.io/keycloak/keycloak:13.0.0"

        ports = ["keycloak"]
      }

      resources {
        cpu = 1024
        memory = 1024
      }

      template {
        data = <<EOH
          TZ='Europe/Stockholm'

          KEYCLOAK_USER=admin
          KEYCLOAK_PASSWORD=admin

          PROXY_ADDRESS_FORWARDING=true

          {{ with secret "identity-access-management/keycloak" }}
          DB_VENDOR=mysql
          DB_ADDR=192.168.20.99
          DB_PORT=27181
          DB_DATABASE=keycloak
          DB_USER="{{ index .Data.data "MYSQL_USER" }}"
          DB_PASSWORD="{{ index .Data.data "MYSQL_PASS" }}"
          {{ end }}
        EOH

        destination = "secrets/keycloak.env"
        env = true
      }

      service {
        name = "Keycloak"
        port = "keycloak"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.keycloak.entrypoints=https",
          "traefik.http.routers.keycloak.rule=Host(`keycloak.hq.carboncollins.se`)",
          "traefik.http.routers.keycloak.tls=true",
          "traefik.http.routers.keycloak.tls.certresolver=lets-encrypt",
          "traefik.http.routers.keycloak.tls.domains[0].main=keycloak.hq.carboncollins.se",
          "hq.service.exposed=true",
          "hq.service.subdomain=keycloak"
        ]

        check {
          name = "Keycloak"
          port = "keycloak"
          address_mode = "driver"
          type = "tcp"
          interval = "30s"
          timeout = "15s"
          task = "keycloak"
        }
      }
    }
  }
}